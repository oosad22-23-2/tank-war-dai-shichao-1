package ss;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Objects;
import java.util.Random;

//tank类
public class Tank {
    public static final int XSPEED = 5;
    public static final int YSPEED = 5;

    public static final int WIDTH = 30;
    public static final int HEIGHT = 30;

    public static final int LIFE = 100;

    //定义一个随机数产生器，此时的Random类是java.util.Random，不同于Math中的
    private static Random r = new Random(); //随机数产生器只需要一个，所以定义成静态，防止每次new一个坦克是都会产生一个随机数产生器

    private BloodBar bb = new BloodBar();
    private int x, y;//定义变量画圆圈(坦克)时四边形左上点的x、y左边
    private int oldX, oldY; //定义坦克上个位置的坐标


    //定义变量说明是我方还是敌方坦克，为true表示我方坦克子弹
    private boolean good;


    //定义变量说明是坦克是否存活

    private boolean live = true;


    //设置坦克的生命值为100
    private int life = LIFE;

    //保留TankClient的引用，更方便地使用其中的成员变量
    TankClient tc;

    //是否按下四个方向键
    private boolean bL = false,
            bU = false,
            bR = false,
            bD = false;

    public Tank(int x, int y, boolean good) {
        this.x = x;
        this.y = y;
        this.good = good;
        this.oldX = x;
        this.oldY = y;
    }

    public Tank(int x, int y, boolean good, TankClient tc) {
        this(x, y, good); //相当于调用上面的构造方法
        this.tc = tc;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    private void stay() {
        x = oldX;
        y = oldY;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    private class BloodBar {
        public void draw(Graphics g) {
            Color c = g.getColor();
            g.setColor(Color.RED);
            g.drawRect(x, y - 10, WIDTH, 10);
            int w = WIDTH * life / 100;
            g.fillRect(x, y - 10, w, 10);
            g.setColor(c);

        }
    }


    //成员变量：方向
    //定义枚举类型，值为左、左上、上、右上、右、右下、下、左下、停止
    enum Direction {D, L, LD, LU, R, RD, RU, STOP, U}


    //看不懂

    private Direction dir = Direction.STOP;//定义变量坦克的方向

    //ptDir代表炮筒的方向，默认方向向下
    private Direction ptDir = Direction.D;

    //宽度高度

    private int step = r.nextInt(12) + 3; //定义坦克朝着一个方向移动几步

    //画个tank
    public void draw(Graphics g) {
        if (!live) {
            if (!good) {
                tc.tanks.remove(this);
            }
            return; //如果坦克没有存活就直接返回，不用画坦克了
        }

        Color c = g.getColor(); //取得g(以后称为画笔)的颜色

        //用不同颜色区分敌我
        if (good) {
            g.setColor(Color.RED);
        } else {
            g.setColor(Color.BLUE);
        }

        g.fillOval(x, y, WIDTH, HEIGHT);//"画圆",利用填充一个四边形(四边形的内切圆)，参数分别代表：四边形左上点的坐标X，Y，宽度，高度
        g.setColor(c);//用完画笔后把画笔默认的颜色(黑色)设置回去

        if (good) {
            bb.draw(g);
        }


        //判断出炮筒的方向，并模拟方向来画出炮筒
        if (Objects.requireNonNull(ptDir) == Direction.L) g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT /
                2, x, y + Tank.HEIGHT / 2);
        else if (ptDir == Direction.LU) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT /
                    2, x, y);
        } else if (ptDir == Direction.U) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT /
                    2, x + Tank.WIDTH / 2, y);
        } else if (ptDir == Direction.RU) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT /
                    2, x + Tank.WIDTH, y);
        } else if (ptDir == Direction.R) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH, y + Tank.HEIGHT / 2);
        } else if (ptDir == Direction.RD) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH, y + Tank.HEIGHT);
        } else if (ptDir == Direction.D) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x + Tank.WIDTH / 2, y + Tank.HEIGHT);
        } else if (ptDir == Direction.LD) {
            g.drawLine(x + Tank.WIDTH / 2, y + Tank.HEIGHT / 2, x, y + Tank.HEIGHT);
        }
        move();//每次按键都会重画，就会调用drawTank，在这里重画坦克的此时位置
    }

    //移动，看不懂，太高级啦
    void move() {
        this.oldX = x;
        this.oldY = y;

        if (Objects.requireNonNull(dir) != Direction.L) {
            if (dir == Direction.LU) {
                x -= XSPEED;
                y -= YSPEED;
            } else if (dir == Direction.U) {
                y -= YSPEED;
            } else if (dir == Direction.RU) {
                x += XSPEED;
                y -= YSPEED;
            } else if (dir == Direction.R) {
                x += XSPEED;
            } else if (dir == Direction.RD) {
                x += XSPEED;
                y += YSPEED;
            } else if (dir == Direction.D) {
                y += YSPEED;
            } else if (dir == Direction.LD) {
                x -= XSPEED;
                y += YSPEED;
            }
        } else {
            x -= XSPEED;
        }
        //将坦克的方向传给炮筒，使炮筒与坦克方向一致
        if (this.dir != Direction.STOP) {
            this.ptDir = this.dir;
        }

        //防止坦克出界
        if (x < 0) x = 0;
        if (y < 25) y = 25;
        if (x + Tank.WIDTH > TankClient.GAME_WIDTH) x = TankClient.GAME_WIDTH - Tank.WIDTH;
        if (y + Tank.HEIGHT > TankClient.GAME_HEIGHT) y = TankClient.GAME_HEIGHT - Tank.HEIGHT;
        if (!good) {
            Direction[] dirs = Direction.values(); //把枚举转换成数组
            if (step == 0) {
                int rn = r.nextInt(dirs.length);
                dir = dirs[rn]; //如果移动步数为0就改变方向
                step = r.nextInt(12) + 3;
            }
            step--;

            if (r.nextInt(40) > 37) this.fire();
        }

    }

    //键盘监视器

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode(); //得到按键的虚拟码，再和下面的KeyEvent.VK_LEFT等虚拟码比较看是否是某按键
        switch (key) {
            case KeyEvent.VK_LEFT -> bL = true;
            case KeyEvent.VK_UP -> bU = true;
            case KeyEvent.VK_RIGHT -> bR = true;
            case KeyEvent.VK_DOWN -> bD = true;
        }
        locateDirection();
        if (dir != Direction.STOP) {
            ptDir = dir;
        }
    }

    //键盘监视器反着来
    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        //按下Ctrl时作出动作
        //只有当键被抬起来时，这枚炮弹才被发出去
        //避免了一直按下Ctrl而使得子弹过多的问题
        switch (key) {
            case KeyEvent.VK_CONTROL:
                fire();
                break;
            case KeyEvent.VK_LEFT:
                bL = false;
                break;
            case KeyEvent.VK_UP:
                bU = false;
                break;
            case KeyEvent.VK_RIGHT:
                bR = false;
                break;
            case KeyEvent.VK_DOWN:
                bD = false;
                break;
            case KeyEvent.VK_A:
                superFire();
                break;
        }
        locateDirection();
        if (dir != Direction.STOP) {
            ptDir = dir;
        }
    }

    //通过上右下的按键是否被按下判断坦克要运动的方向
    void locateDirection() {
        if (bL && !bU && !bR && !bD) dir = Direction.L;
        else if (bL && bU && !bR && !bD) dir = Direction.LU;
        else if (!bL && bU && !bR && !bD) dir = Direction.U;
        else if (!bL && bU && bR && !bD) dir = Direction.RU;
        else if (!bL && !bU && bR && !bD) dir = Direction.R;
        else if (!bL && !bU && bR) dir = Direction.RD;
        else if (!bL && !bU && bD) dir = Direction.D;
        else if (bL && !bU && !bR) dir = Direction.LD;
        else if (!bL && !bU) dir =
                Direction.STOP;

    }

    public Missile fire() {
        if (!live) return null;
        //保证子弹从Tank的中间出现
        int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;//让子弹从中心打出
        int y = this.y + Tank.HEIGHT / 2 - Missile.WIDTH / 2;
        //将Tank现在的位置和方向传递给子弹
        //并且现在子弹的初始化不再是由坦克决定，而是由炮筒决定
        Missile m = new Missile(x, y, ptDir, this.tc, good);
        //在这里将missile加入到容器里
        tc.missiles.add(m);//每new一个Missile对象就把他装到集合中
        return m; //返回的m，其他地方可调用可不调用
    }

    public Missile fire(Direction dir) {
        if (!live) return null;
        //保证子弹从Tank的中间出现
        int x = this.x + Tank.WIDTH / 2 - Missile.WIDTH / 2;
        int y = this.y + Tank.HEIGHT / 2 - Missile.WIDTH / 2;
        //将Tank现在的位置和方向传递给子弹
        //并且现在子弹的初始化不再是由坦克决定，而是由炮筒决定
        Missile m = new Missile(x, y, dir, this.tc, good);
        tc.missiles.add(m);
        return m;
    }


    public Rectangle getRect() {
        return new Rectangle(x, y, WIDTH, HEIGHT); //得到坦克的探测方块，Rectangle是java.awt包中专门用于游戏碰撞的类
    }

    public boolean isLive() {
        return live;
    }

    //Tank类的成员方法可生成对应的get和set方法，那么在其他类中就可以访问了
    public void setLive(boolean live) {
        this.live = live;
    }


    public boolean isGood() {
        return good;
    }

    //检测坦克是否相撞,java.util.List<E>接口或者类的另一种写法，java.awt中也有List，所以要写明确
    public boolean collidesWithTanks(java.util.List<Tank> tanks) {
        for (int i = 0; i < tanks.size(); i++) {
            Tank t = tanks.get(i);
            if (this != t) {
                if (this.live && t.isLive() && this.getRect().intersects(t.getRect())) {
                    this.stay();
                    t.stay();
                    return true;
                }
            }
        }
        return false;
    }

    //超级炮弹：朝8个方向各发一发炮弹
    private void superFire() {
        Direction[] dirs = Direction.values();
        for (int i = 0; i < 8; i++) { //dirs[8]是STOP
            fire(dirs[i]);
        }
    }


    //检测坦克是否撞墙
    public boolean collidesWithWall(Wall w) {
        if (this.live && this.getRect().intersects(w.getRect())) {
            stay(); //如果坦克撞到墙就让他回到上一个位置
            return true;
        }
        return false;
    }
    public boolean eat(Blood b) {
        if(this.live && b.isLive() &&
                this.getRect().intersects(b.getRect())) { this.life = 100; b.setLive(false);
            return true;
        }
        return false;
    }


}
