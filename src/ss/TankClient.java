package ss;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;


public class TankClient extends Frame {
    //主函数
    public static void main(String[] args) {
        TankClient tc = new TankClient();
        tc.launchFrame();
    }
    //代码重构优化，将以后需要多处改变的量定义为常量。比如Frame的宽度和高度
    public static final int GAME_WIDTH = 800;
    public static final int GAME_HEIGHT = 600;
    //将位置改为变量
    //创建tank实体类
    Tank myTank = new Tank(50, 50, true, this);
    Wall w1 = new Wall(100, 300, 20, 100, this),
            w2 = new Wall(300, 250, 150, 20, this);
    //定义集合来装子弹
    List<Missile> missiles = new ArrayList<>();
    List<Explode> explodes = new ArrayList<>();
    List<Tank> tanks = new ArrayList<>();


    //这是一张虚拟图片
    Image offScreenImage = null;

    //代表坦克的实心圆
    //paint这个方法不需要被主动调用，一旦重新画时会被自动调用
    @Override
    public void paint(Graphics g) {
        //显示出容器中装了多少炮弹
        g.drawString("missiles count: " + missiles.size(), 10, 50);
        //显示有多少个爆炸
        g.drawString("explodes count: " + explodes.size(), 10, 70);
        //显示敌方坦克数量
        g.drawString("tanks count: " + tanks.size(), 10, 90);


        //将容器中的炮弹逐个画出来
        for (int i = 0; i < missiles.size(); i++) {
            Missile m = missiles.get(i);
            m.hitTanks(tanks);
            m.hitTank(myTank);

            m.hitWall(w1); //检测子弹是否撞墙
            m.hitWall(w2);
            m.draw(g);

            //if(!m.isLive()) missiles.remove(m);
            //else m.draw(g);
        }

        //将容器中的爆炸逐个画出来
        for (int i = 0; i < explodes.size(); i++) {
            Explode e = explodes.get(i);
            e.draw(g);
        }
        //new坦克的循环
        for (int i = 0; i < tanks.size(); i++) {
            Tank enemyTank = tanks.get(i);
            enemyTank.collidesWithWall(w1); //检测敌方坦克是否撞墙
            enemyTank.collidesWithWall(w2);
            enemyTank.collidesWithTanks(tanks);
            enemyTank.draw(g);

        }
        myTank.draw(g);
        w1.draw(g);
        w2.draw(g);
    }


    //看不懂，太高级的双缓冲工作原理
    @Override
    public void update(Graphics g) {
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);//判断是为了避免每次重画时都给offScreenImage赋值
        }
        //拿到这个图片的画笔
        Graphics gOffScreen = offScreenImage.getGraphics(); //定义虚拟图片上的画笔gOffScreen
        Color c = gOffScreen.getColor();
        gOffScreen.setColor(Color.GREEN);
        //重画背景，如果没有这句则在屏幕上会保留圆圈的移动路径
        gOffScreen.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        gOffScreen.setColor(c);
        //把圆圈画到虚拟图片上
        print(gOffScreen);
        //再一次性把虚拟图片画到真实屏幕上，在真实屏幕上画则要用真实屏幕的画笔g
        g.drawImage(offScreenImage, 0, 0, null);
    }


    //产生一个窗口
    public void launchFrame() {
        for (int i = 0; i < 10; i++) {
            tanks.add(new Tank(50 + 40 * (i + 1), 50, false, this));

        }

        this.setLocation(300, 50);
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        setTitle("TankWar");
        this.setResizable(false); //不允许改变窗口大小
        //添加关闭窗口的事件处理
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });//添加关闭功能，此处使用匿名类比较合适
        //背景色
        this.setBackground(Color.GREEN);
        //键盘监视器
        this.addKeyListener(new KeyMonitor());

        setVisible(true);

        //启动线程，实例化线程对象时不要忘了new Thread(Runnable对象);
        new Thread(new PaintThread()).start();

    }

    //PaintThread只为TankClient服务，所以写成内部类好些
    private class PaintThread implements Runnable {
        public void run() {
            do {
                //repaint()是TankClient或者他的父类的方法，内部类可以访问外部包装类的成员，这也是内部类的好处
                repaint();
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (true);
        }

    }

    //键盘监视器类
    private class KeyMonitor extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            myTank.keyReleased(e);
        }

        @Override
        public void keyPressed(KeyEvent e) {
            myTank.keyPressed(e);
        }


    }
}